# Materialsammlung


- Regelungen für die Anmeldeprozedur eines MA. Ausgabe von HW, Aktivieren von Konten, Erteilung des VPN-Zugangs

- Regelungen für die Abmeldeprozedur eines MA. So Sachen wie die Rückgabe von HW, Deaktivieren von Konten, Rücknahme des VPN-Zugangs.

- Und was ist mit der Übergabe von Daten an den Gruppenleiter? Wie lange werden Daten aufgehoben? Soll sowas in die BO, oder an anderer (welcher?) Stelle aufgeschrieben werden?

- 
