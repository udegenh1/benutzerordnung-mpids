---
lang: de-DE

numbersections: false

---

## Nutzungsordnung für IT-Dienste des MPIDS - Erläuterungen zu selbstverwalteten Arbeitsplatzrechnern

## 1. Geltungsbereich

Diese Erläuterungen zur Nutzungsordnung gelten für die dienstliche Nutzung selbstverwalteter Arbeitsplatzrechner des Max-Planck-Instituts für Dynamik und Selbstorganisation.

### 2. Erläuterungen

Arbeitsplatzrechner des MPIDS können bei Bedarf und unter Vorlage einer schriftlichen Begründung und einer schriftlichen Genehmigung des Vorgesetzten eines Mitarbeiters selbst administriert werden. Der typische Anwendungsfall ist die Verwendung eines Notebooks durch einen Mitarbeiter. 

Liegen alle Voraussetzungen vor, kann der Mitarbeiter administrativen Zugriff auf den Arbeitsplatzrechner bekommen. Dann gelten zusätzlich zu den Regularien der Benutzerordnung die folgenden Regeln:

- Der Arbeitsplatzrechner unterliegt weiterhin der zentralen Verwaltung von Hardware und Software, die von der IT-Gruppe zur Verfügung gestellt wird. Die zentrale Verwaltung darf nicht deaktiviert werden.

- Das Betriebssystem des Arbeitsplatzrechners ist selbstständig und ohne weitere Aufforderung immer dann vom Benutzer zu aktualisieren, wenn eine neue Softwareversion vom Hersteller bereitgestellt wird.

- Installierte Anwendungssoftware ist selbstständig und ohne weitere Aufforderung zu aktualisieren, wenn neue Versionen vom Hersteller bereitgestellt werden.

- ...

Nichtbefolgen dieser Regeln kann das temporäre Sperren des Arbeitsplatzrechners durch die IT-Gruppe zur Folge haben. Die Sperrung wird wieder aufgehoben, wenn der Arbeitsplatzrechner den Anforderungen der hier formulierten Regeln entspricht.


