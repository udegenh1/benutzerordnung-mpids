---
lang: de-DE
numbersections: false
---

# Nutzungsordnung für IT-Dienste des Max-Planck-Instituts für Dynamik und Selbstorganisation

## 1. Ziel

Aufgabe des Instituts ist es, eine leistungsfähige, zuverlässige,
sichere, und gesetzeskonforme IT-Infrastruktur im Dienste der
Wissenschaft einzurichten und zu betreiben. Dazu bedarf es der
Kooperation und aktiven Mitarbeit aller Nutzer\*innen.

## 2. Geltungsbereich

- Diese Nutzungsordnung gilt für die Nutzung der IT-Infrastruktur
  (Netzwerk, IT-Systeme, Anwendungen, etc.) aller Standorte des
  Max-Planck-Instituts für Dynamik und Selbstorganisation.

- Die Nutzungsordnung gilt auch für die dienstliche Nutzung privater
  IT-Geräte, sowie beim Einsatz dienstlicher Geräte außerhalb des
  Instituts.

- Alle Nutzer\*innen der IT-Infrastruktur -- unabhängig von der Art ihres
  Rechtsverhältnisses zur MPG -- sind verpflichtet, die Regelungen
  dieser Nutzungsordnung einzuhalten.

- Soweit gesetzliche Bestimmungen inkl.
  (Gesamt-)Betriebsvereinbarungen und/oder vertragliche Bestimmungen
  Rechte oder Pflichten für Beschäftigte der MPG abweichend von dieser
  Nutzungsordnung regeln, haben diese Regelungen Vorrang.

## 3. Nutzungsberechtigung

- Die Nutzungsberechtigung erfolgt auf Antrag. Sie kann zeitlich
  befristet und auf bestimmte IT-Ressourcen eingeschränkt werden.

- Die Zulassung zur Nutzung erfolgt ausschließlich zur Erfüllung
  dienstlicher oder vertraglicher Aufgaben oder zu wissenschaftlichen
  Zwecken in Forschung, Lehre und Ausbildung, soweit die 
  Max-Planck-Gesellschaft eine abweichende Nutzung nicht ausdrücklich
  erlaubt.

- Nutzer\*innen kann aus wichtigem Grund die Nutzungsberechtigung
  entzogen werden. Hierüber entscheidet die Institutsleitung.

## 4. Nutzungsbedingungen

### Allgemein

- Die Nutzer\*innen haben alle rechts- oder ordnungswidrigen,
  unerlaubten oder unangemessenen Nutzungshandlungen und
  Verhaltensweisen zu unterlassen. Sie haben darüber hinaus alle
  Nutzungshandlungen und Verhaltensweisen zu unterlassen, welche
  geeignet sind, Schäden oder sonstige Nachteile für die
  Max-Planck-Gesellschaft oder deren Institute herbeizuführen oder das
  Ansehen bzw. die Interessen der Max-Planck-Gesellschaft oder deren
  Institute zu beschädigen bzw. zu verletzen.

- Vom Institut zur Verfügung gestellte Geräte sind pfleglich zu
  behandeln. Veränderungen an den Geräten müssen mit der IT vorher abgestimmt
  werden.

- Dienstliche Hardware oder Software darf Dritten nicht zur Nutzung
  überlassen werden.

### Schutz der Infrastruktur

- Bei Verdacht auf Eindringen von Unbefugten oder Schadsoftware im System
  ist die IT unverzüglich, d.h. ohne schuldhaftes Zögern, zu informieren.
  Sonstige IT-Probleme sollten der IT gemeldet werden.

- Zur Störungsbeseitigung, aus Gründen der Systemadministration und
  -sicherheit oder zum Schutz der Nutzerdaten können die Nutzung der
  Ressourcen vorübergehend eingeschränkt oder einzelne Nutzerkennungen
  vorübergehend gesperrt werden. Die IT bemüht sich, betroffene
  Nutzer\*innen hierüber im Voraus zu unterrichten.

- Die Nutzung der IT-Infrastruktur kann protokolliert und ausgewertet
  werden, jedoch nur soweit dies erforderlich ist

  1. zur Gewährleistung eines ordnungsgemäßen Systembetriebs oder

  2. zur Ressourcenplanung und Systemadministration oder

  3. zum Schutz der personenbezogenen Daten anderer Nutzer oder

  4. zu Abrechnungszwecken oder

  5. für das Erkennen und Beseitigen von Störungen

- Die Sicherheit der Nutzerpasswörter und -daten können durch
  regelmäßige Maßnahmen überprüft und notwendige Schutzmaßnahmen
  durchgeführt werden. Bei erforderlichen nutzungsrelevanten
  Schutzmaßnahmen werden die betroffenen Nutzer\*innen unverzüglich
  informiert.

### Software

- Installation oder Nutzung von zusätzlicher Software sollte mit der
  IT abgestimmt werden.

- Nutzer\*innen dürfen nur korrekt lizensierte Software auf den
  Rechnern des Institutes installieren und nutzen.

### Zugriffskontrolle

- Soweit technisch möglich, sind beim Verlassen des Arbeitsplatzes
  Rechner und Endgeräte mit einem Kennwortschutz, PIN, Fingerabdruck
  oder einem anderen Verfahren vor unberechtigtem Zugriff zu schützen.
  Begründete Ausnahmen sind möglich, falls die Arbeitsorganisation es
  dringend erfordert (z.B. Mess- und Steuerrechner).

- Die Speicherung von schützenswerten Daten auf mobilen Endgeräten
  oder Speichermedien ist nur dann zulässig, wenn diese verschlüsselt
  sind.

### Umgang mit Zugangsdaten

- **Geheimhaltung:** Die eigenen, geheimen Zugangsdaten (Passwörter,
  private Schlüssel, Token) müssen vertraulich behandelt werden und
  dürfen keiner anderen Person mitgeteilt werden. Die Eingabe muss
  unbeobachtet stattfinden.

- **Verdacht auf Kompromittierung:** Bei Anzeichen einer möglichen
  Kompromittierung der Zugangsdaten muss der/die Nutzer\*in diese
  unverzüglich ändern und den IT-Service benachrichtigten.

- **Starke Passwörter:** Falls Passwörter zur Authentifizierung
  verwendet werden, sollten diese stark sein, d.h.

  - ausreichend lang, zufällig und komplex sein (mindestens 10
    Stellen, besser mehr)
  - auf keinen Sachverhalten basieren, die eine andere Person unter
    Zuhilfenahme personenbezogener Daten wie z.B. Namen,
    Telefonnummern, Geburtstage usw. einfach erraten oder
    erschließen kann

- Passwörter sollten

  - nicht im Klartext gespeichert oder notiert werden
  - sich für verschiedene Benutzerkonten unterscheiden; das gilt
    insbesondere für externe Konten
  - zweckmäßigerweise in einem Passwort-Manager generiert und
    verwaltet werden
  - bei der ersten Anmeldung geändert werden, wenn es sich um
    initiale Passwörter handelt

- Weitere Hinweise und Beispiele finden sich in den "Passwort-Regeln
  der Max-Planck-Gesellschaft"

### Umgang mit Daten

- Informationen werden nach den vier Kategorien "öffentlich",
  "intern", "vertraulich" und "streng vertraulich" klassifiziert.
  Details regelt die "Richtlinie zur Klassifikation von
  Informationen".

- Dienstliche Daten sind grundsätzlich auf IT-Systemen des Instituts
  oder anderen vom Institut zugelassenen IT-Systemen zu speichern. In
  begründeten Ausnahmefällen, soweit dienstlich unbedingt
  erforderlich, können auch andere Systeme (externe Cloud-Dienste,
  private Geräte) verwendet werden, solange und insofern sie dem
  jeweiligen Schutzbedarf gerecht werden und die Cloud-Policy der MPG
  eingehalten wird.

- Wichtige Daten sind nur auf Systemen zu speichern, für die ein
  Backup sichergestellt ist.

- Eine Verarbeitung personenbezogener Daten mit erhöhtem Schutzbedarf
  muss mit dem/der Datenschutzkoordinator\*in abgestimmt werden. Die
  Verantwortung dafür obliegt dem/der jeweiligen Nutzer\*in.

- Vor Ablauf der Nutzungsberechtigung müssen alle dienstlichen Daten
  dem/der Vorgesetzten geordnet übergeben werden. Hierbei sind
  insbesondere die "Regeln zur Sicherung guter wissenschaftlicher
  Praxis" einzuhalten. Evtl. vorhandene nicht dienstliche,
  insbesondere private Daten müssen gelöscht werden.

- Sofern die Daten und Programme im Institut keine weitere Verwendung
  finden, können diese nach Ablauf der Nutzungsberechtigung gesperrt,
  archiviert oder gelöscht werden.

### Kommunikationsdienste

Zum Umgang mit Daten gehört auch die Nutzung von Kommunikations- und
Kollaborationsdiensten. Dementsprechend gilt:

- Vertrauliche Daten dürfen nur verschlüsselt verschickt werden.

- Für Kommunikation mit vertraulichen Daten ist nur die 
  Verwendung dienstlicher Konten zulässig, z.B. E-Mail-Konten.

- Internetlinks und Attachments sollten nur dann geöffnet
  werden, wenn durch Herkunft und Kontext davon ausgegangen werden
  kann, dass sie vertrauenswürdig sind.

- Eine systematische Weiterleitung dienstlicher E-Mails an externe
  Provider oder der Zugriff eines externen Providers auf das
  dienstliche E-Mail-Konto sind nicht zulässig.

- Die Regeln zur geordneten Übergabe dienstlicher und zur Löschung
  privater Daten beim Ausscheiden aus dem Institut (siehe oben) gelten
  auch für Daten in Kommunikationsdiensten.


## 5. Haftung der Nutzenden

- Für die Haftung und die Freistellungspflichten von Nutzer\*innen, die Beschäftigte der MPG sind, 
  gelten die arbeitsvertraglich vereinbarten Haftungsregelungen bzw. die allgemeinen 
  arbeitsrechtlichen Haftungsgrundsätze. Für Nutzer\*innen, die keine Beschäftigte der MPG sind,
  gelten die nachstehenden drei Absätze.

  - Nutzer\*innen haften für alle Schäden und Nachteile, die der MPG durch eine missbräuchliche
  oder rechtswidrige Verwendung der IT-Infrastruktur bzw. dadurch entstehen, dass Nutzer\*innen
  schuldhaft ihren Pflichten aus dieser IT-Nutzungsrichtlinie nicht nachkommen.

  - Nutzer\*innen haften für Schäden, die im Rahmen der ihnen zur Verfügung gestellten 
  Zugriffs- und Nutzungsmöglichkeiten durch Drittnutzung entstanden sind, wenn sie die 
  Drittnutzung zu vertreten haben.

  - Nutzer\*innen haben die MPG von allen Ansprüchen freizustellen, die Dritte gegen die MPG
  aufgrund einer schuldhaften Verletzung ihrer Pflichten aus dieser IT-Nutzungsrichtlinie
  geltend machen.

## 6. Haftung der MPG-Einrichtung

- Die MPG übernimmt keine Garantie dafür, dass die IT-Infrastruktur jederzeit fehlerfrei funktioniert.
  Eventuelle Datenverluste infolge technischer Störungen sowie die Kenntnisnahme von Daten durch
  unberechtigte Zugriffe Dritter können nicht gänzlich ausgeschlossen werden.

- Die MPG übernimmt keine Verantwortung für die Richtigkeit der zur Verfügung gestellten Programme.
  Die MPG haftet nicht für den Inhalt, für die Richtigkeit, Vollständigkeit und Aktualität der
  Informationen, zu denen sie lediglich den Zugang zur Nutzung vermittelt.

- Im Übrigen haftet die Max-Planck-Gesellschaft für Schäden, die keine Personenschäden sind,
  nur bei Vorsatz und grober Fahrlässigkeit. Dies gilt auch bei leicht fahrlässigen 
  Pflichtverletzungen gesetzlicher Vertreter oder Beschäftigter.

## 7. Aktualisierungen

Die Nutzungsordnung wird mindestens alle 5 Jahre oder anlassbezogen
aktualisiert. Änderungen werden den Nutzern\*innen kommuniziert.

Diese Nutzungsordnung tritt am xx.xx.xxxx in Kraft.
